# Mini Stories

| id | story | remarks |
| --- | --- | --- |
| 1 | librarian should be able to add books | [0.5] Can add books, but no authentication |
| 2 | public should be able to borrow books that are not on loan | [x] |
| 3 | public should not be able to borrow books on loan | [x] |
| 4 | public should be able to see book list and availabilities | [x] |
| 5 | public should be able to return loans | [x] |

## Features

[x] mini menu

[] loan view statuses: on loan, returned, overdue

[] database check that book is not already loaned

[] when there is no current user book list should not show read option

# Model Design

## Person

| key  |  type   | constraints |
| --- | --- | --- |
| name | :string | |
| role | :string | librarian or patron |

has_many :bookloan

## Book

| key | type | constraints |
| --- | --- | --- |
| name | :string | |
| isbn | :string | unique |

has_many :bookloan

## BookLoan

| key | type | constraints |
| --- | --- | --- |
| due | :datetime | |

has_one :person
has_one :book

# Running

`rails server -b 0.0.0.0 -p 3000`
`./ngrok http 3000`

# Tests

`rails test`

# Development cheatsheet

migrate db

`rake db:migrate`

reset db

`rake db:reset`

generate controllers

`rails g controller Domain view1 view2 ...`

generate models

`rails g model Model field:field_type ...`

insert seed data
`rails db:seed`

reset and seed for demo
`rails db:reset && rails db:migrate && rails runner demo_seeds.rb`

# Development Notes

Book data from [https://gist.github.com/AdamSteffanick/ecf1240e182fe7d86fd4](adamsteffanick)

# Afterthought

- Books should have authors
- Books have titles, not names
- Books can have duplicate isbns
- `returned` should be `is_returned`
- Only one book loan per book can be not returned, consider some intermediate cache/column to store book loan id

# Ruby/Rails Learning

## Callbacks

Default due date and loan status

## Validation

Due date cannot be null

## erb linter/formatter

what is the solution?