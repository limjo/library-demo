require "test_helper"

class BookLoanTest < ActiveSupport::TestCase
  setup :initialize_people_and_books
  teardown :tear_down

  def tear_down
    BookLoan.delete_all
    Book.delete_all
    Person.delete_all
  end

  def initialize_people_and_books
    books = [
      ["The Glass Castle", "074324754X"],
      ["The Wisdom of Crowds", "385721706"],
      ["The Future of Ideas", "375726446"],
      ["Slaughterhouse-Five", "385333846"],
      ["A Journal of the Plague Year", "375757899"],
    ]

    books.each do |b|
      Book.create({ name: b[0], isbn: b[1] })
    end

    # Person

    people = [
      ["Leslie", :librarian],
      ["Larry", :librarian],
      ["Preston", :public],
      ["Phil", :public],
      ["Patsy", :public],
      ["Paxton", :public],
    ]

    people.each do |p|
      Person.create({ name: p[0], role: p[1] })
    end

    loans = [
      ["Preston", "074324754X"],
    ]

    loans.each do |name, isbn|
      puts name, isbn
      p, b = Person.find_by(name: name), Book.find_by(isbn: isbn)
      loan = BookLoan.new
      loan.person_id = p.id
      loan.book_id = b.id
      loan.save!
    end
  end

  test "insert book loan without due date should pass with default due date" do
    loan = BookLoan.new
    loan.person_id = Person.find_by(name: "Preston").id
    loan.book_id = Book.find_by(isbn: "074324754X").id
    actual = loan.save
    assert actual
    assert BookLoan.find_by(id: loan.id).person.name == "Preston"
  end
end
