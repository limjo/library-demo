require "application_system_test_case"

class BookLoansTest < ApplicationSystemTestCase
  setup do
    @book_loan = book_loans(:one)
  end

  test "visiting the index" do
    visit book_loans_url
    assert_selector "h1", text: "Book Loans"
  end

  test "creating a Book loan" do
    visit book_loans_url
    click_on "New Book Loan"

    fill_in "Book", with: @book_loan.book_id
    fill_in "Due", with: @book_loan.due
    fill_in "Person", with: @book_loan.person_id
    check "Returned" if @book_loan.returned
    click_on "Create Book loan"

    assert_text "Book loan was successfully created"
    click_on "Back"
  end

  test "updating a Book loan" do
    visit book_loans_url
    click_on "Edit", match: :first

    fill_in "Book", with: @book_loan.book_id
    fill_in "Due", with: @book_loan.due
    fill_in "Person", with: @book_loan.person_id
    check "Returned" if @book_loan.returned
    click_on "Update Book loan"

    assert_text "Book loan was successfully updated"
    click_on "Back"
  end

  test "destroying a Book loan" do
    visit book_loans_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Book loan was successfully destroyed"
  end
end
