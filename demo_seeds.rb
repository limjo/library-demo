books = [
  ["The Glass Castle", "074324754X"],
  ["The Wisdom of Crowds", "385721706"],
  ["The Future of Ideas", "375726446"],
  ["Slaughterhouse-Five", "385333846"],
  ["A Journal of the Plague Year", "375757899"],
]

books.each do |b|
  Book.create({ name: b[0], isbn: b[1] })
end

# Person

people = [
  ["Leslie", :librarian],
  ["Larry", :librarian],
  ["Preston", :public],
  ["Phil", :public],
  ["Patsy", :public],
  ["Paxton", :public],
]

people.each do |p|
  Person.create({ name: p[0], role: p[1] })
end

## Loans

# returned
loans = [
  ["Preston", "074324754X"],
  ["Preston", "375757899"],
]

loans.each do |name, isbn|
  puts name, isbn
  p, b = Person.where(name: name).first, Book.where(isbn: isbn).first
  loan = BookLoan.new(person_id: p.id, book_id: b.id, returned: true)
  loan.save
end

# oustanding
loans = [
  ["Paxton", "074324754X"],
  ["Phil", "375726446"],
]

loans.each do |name, isbn|
  puts name, isbn
  p, b = Person.where(name: name).first, Book.where(isbn: isbn).first
  loan = BookLoan.new(person_id: p.id, book_id: b.id)
  loan.save
end
