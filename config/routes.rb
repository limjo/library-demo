Rails.application.routes.draw do
  root "books#index"
  resources :book_loans
  resources :people, as: "people"
  resources :books, as: "books"
  get 'logout', to: "login#logout"
  get 'login', to: "login#login_form"
  post 'login', to: "login#login"
  post 'loan_book', to: "book_loans#loan_book"
  patch 'return_book', to: "book_loans#return_book"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
