class LoginController < ApplicationController
    def login_form

    end

    def logout
        session.delete(:current_user_id)
        redirect_to :root
    end

    def login
        person = Person.find_by(name: params[:person][:name])
        if person
            session[:current_user_id] = person.id
            redirect_to :root
        else
            redirect_to :login_path
        end
    end
end
