class BookLoan < ApplicationRecord
  after_initialize :ensure_values_are_present
  validates :due, presence: true
  validates :returned, inclusion: { in: [true, false] }
  validates :book_id, presence: true
  validates :person_id, presence: true
  
  belongs_to :person
  belongs_to :book

  def ensure_values_are_present
    if self.new_record?
      if !!self.returned != self.returned
        self.returned = false
      end
      self.due ||= Time.now + 2.months
    end
  end
end
