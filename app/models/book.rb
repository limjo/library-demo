class Book < ApplicationRecord
  has_many :book_loans, -> { where(returned: false) }
end
