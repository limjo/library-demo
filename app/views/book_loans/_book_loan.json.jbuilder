json.extract! book_loan, :id, :due, :returned, :person_id, :book_id, :created_at, :updated_at
json.url book_loan_url(book_loan, format: :json)
