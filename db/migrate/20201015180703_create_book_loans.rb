class CreateBookLoans < ActiveRecord::Migration[6.0]
  def change
    create_table :book_loans do |t|
      t.datetime :due
      t.boolean :returned
      t.integer :person_id
      t.integer :book_id

      t.timestamps
    end
  end
end
